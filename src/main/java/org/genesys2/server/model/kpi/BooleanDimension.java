/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.kpi;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.genesys2.server.model.genesys.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * BooleanDimension is a filter applied to the query to select matching records
 * for calculation of a {@link Parameter} with Boolean filters: true or false.
 * 
 * <p>
 * <b>Example:</b> To count only accessions with mlsStat==true, a "trueOnly"
 * {@link BooleanDimension} should be configured.
 * </p>
 * 
 * <p>
 * There are not that many useful combinations of {@link BooleanDimension}. Most
 * commonly all three possible values will be used: null, true, and false.
 * </p>
 * 
 * @author mobreza
 *
 */
@Entity
public class BooleanDimension extends Dimension<Boolean> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3020217595599317375L;
	@Column
	private int mode = 3;

	@JsonIgnore
	@Override
	public Set<Boolean> getValues() {
		Set<Boolean> b = new HashSet<Boolean>();
		if (hasTrue()) {
			b.add(Boolean.TRUE);
		}
		if ((mode ^ 2) > 0) {
			b.add(Boolean.FALSE);
		}
		return b;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public boolean hasTrue() {
		return (mode & 1) > 0;
	}

	public void useTrue(boolean use) {
		if (use)
			mode |= 1;
		else
			mode &= ~1;
	}

	public boolean hasFalse() {
		return (mode & 2) > 0;
	}

	public void useFalse(boolean use) {
		if (use)
			mode |= 2;
		else
			mode &= ~2;
	}

}
