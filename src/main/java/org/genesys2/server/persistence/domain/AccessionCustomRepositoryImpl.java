/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class AccessionCustomRepositoryImpl implements AccessionCustomRepository, InitializingBean {
	public static final Log LOG = LogFactory.getLog(AccessionCustomRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	private CriteriaBuilder criteriaBuilder;

	@Override
	public List<Accession> find(FaoInstitute institute, List<String> acceNumbs, List<String> genera) throws NonUniqueAccessionException {
		boolean uniqueAcceNumbs = institute.hasUniqueAcceNumbs();

		CriteriaQuery<Accession> cq = criteriaBuilder.createQuery(Accession.class);
		Root<Accession> root = cq.from(Accession.class);
		cq.select(root);
		root.fetch("stoRage", JoinType.LEFT);

		Join<Accession, Taxonomy2> tax = null;
		if (!uniqueAcceNumbs) {
			tax = root.join("taxonomy");
		}
		List<Predicate> restrictions = new ArrayList<Predicate>();

		if (uniqueAcceNumbs) {
			restrictions.add(root.get("accessionName").in(acceNumbs));
			if (LOG.isDebugEnabled())
				LOG.debug("*** " + institute.getCode() + " " + acceNumbs);
		} else {
			// A lot of .. (acceNumb=? and genus=?)
			for (int i = 0; i < acceNumbs.size(); i++) {
				restrictions.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("accessionName"), acceNumbs.get(i)),
						criteriaBuilder.equal(tax.get("genus"), genera.get(i))));
			}
		}
		cq.where(criteriaBuilder.and(criteriaBuilder.equal(root.get("institute"), institute), criteriaBuilder.or(restrictions.toArray(new Predicate[] {}))));
		cq.distinct(true);
		List<Accession> res = em.createQuery(cq).getResultList();

		if (LOG.isDebugEnabled())
			LOG.debug("*** Loaded accessions " + res.size() + " of " + acceNumbs.size());

		// Check for duplicate names if institute is using unique acceNumbs
		if (uniqueAcceNumbs) {
			Set<String> s = new HashSet<String>();
			for (Accession a : res) {
				if (s.contains(a.getAccessionName())) {
					LOG.error("Duplicate accession name instCode=" + a.getInstituteCode() + " acceNumb=" + a.getAccessionName());
					throw new NonUniqueAccessionException(a.getInstituteCode(), a.getAccessionName());
				}

				s.add(a.getAccessionName());
			}
		}

		return res;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.criteriaBuilder = em.getCriteriaBuilder();
	}
}
