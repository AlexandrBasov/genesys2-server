/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import org.genesys2.server.model.genesys.MaterialRequest;
import org.genesys2.server.model.genesys.MaterialSubRequest;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.RequestService;
import org.genesys2.server.service.RequestService.NoPidException;
import org.genesys2.server.service.impl.EasySMTAException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0" })
public class RequestsController extends RestController {

	private static final int PAGE_SIZE = 10;

	@Autowired
	private RequestService requestService;

	@Autowired
	private InstituteService instituteService;

	/**
	 * List all requests
	 *
	 * @return
	 */
	@RequestMapping(value = "/requests", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listRequests(@RequestParam(value = "page", required = false, defaultValue = "0") int page) {
		LOG.info("Listing requests");
		final Page<MaterialRequest> requests = requestService.list(new PageRequest(page, PAGE_SIZE, new Sort(Direction.DESC, "createdDate")));
		return requests;
	}

	/**
	 * Get request
	 *
	 * @return
	 */
	@RequestMapping(value = "/requests/r/{uuid:.{36}}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	MaterialRequest getRequest(@PathVariable("uuid") String uuid) {
		LOG.info("Loading request uuid=" + uuid);
		final MaterialRequest request = requestService.get(uuid);
		return request;
	}

	/**
	 * Validate request
	 *
	 * @return
	 */
	@RequestMapping(value = "/requests/r/{uuid:.{36}}/reconfirm", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	MaterialRequest reconfirmRequest(@PathVariable("uuid") String uuid) {
		LOG.info("Loading request uuid=" + uuid);
		final MaterialRequest materialRequest = requestService.get(uuid);
		return requestService.sendValidationEmail(materialRequest);
	}

	/**
	 * Validate request
	 *
	 * @return
	 * @throws EasySMTAException 
	 * @throws NoPidException 
	 */
	@RequestMapping(value = "/requests/r/{uuid:.{36}}/validate", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	MaterialRequest validateRequest(@PathVariable("uuid") String uuid) throws NoPidException, EasySMTAException {
		LOG.info("Loading request uuid=" + uuid);
		final MaterialRequest materialRequest = requestService.get(uuid);
		return requestService.validateRequest(materialRequest);
	}

	/**
	 * Reload PID data
	 *
	 * @return
	 * @throws EasySMTAException 
	 * @throws NoPidException 
	 */
	@RequestMapping(value = "/requests/r/{uuid:.{36}}/update-pid", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	MaterialRequest updatePid(@PathVariable("uuid") String uuid) throws NoPidException, EasySMTAException {
		LOG.info("Loading request uuid=" + uuid);
		final MaterialRequest materialRequest = requestService.get(uuid);
		return requestService.recheckPid(materialRequest);
	}

	/**
	 * List institute requests
	 *
	 * @return
	 */
	@RequestMapping(value = "/requests/{instCode}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listInstituteRequests(@PathVariable("instCode") String instCode, @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
		LOG.info("Listing requests for " + instCode);
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		final Page<MaterialSubRequest> requests = requestService.list(institute, new PageRequest(page, PAGE_SIZE, new Sort(Direction.DESC, "createdDate")));
		return requests;
	}

	/**
	 * Get institute request
	 *
	 * @return
	 */
	@RequestMapping(value = "/requests/{instCode}/r/{uuid:.{36}}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object getInstituteRequest(@PathVariable("instCode") String instCode, @PathVariable("uuid") String uuid) {
		LOG.info("Loading request for " + instCode + " uuid=" + uuid);
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		final MaterialSubRequest request = requestService.get(institute, uuid);
		return request;
	}

}
