<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="resolver.page.reverse.title" /></title>
</head>
<body>
  <h1>
    <spring:message code="resolver.page.reverse.title" />
  </h1>

  <div class="crop-details">
    <div class="row header not-important">
      <div class="col-sm-2">
        <spring:message code="accession.holdingInstitute" />
      </div>
      <div class="col-sm-2">
        <spring:message code="accession.accessionName" />
      </div>
      <div class="col-sm-4">
        <spring:message code="accession.taxonomy" />
      </div>
    </div>
    <c:forEach items="${accessions}" var="accession" varStatus="status">
      <div class="row ${accession.getClass().simpleName=='AccessionHistoric' ? 'text-muted' : ''}">
        <div class="col-sm-2">
          <a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a>
        </div>
        <div class="col-sm-2">
          <c:choose>
            <c:when test="${accession.getClass().simpleName=='AccessionHistoric'}">
              <a href="<c:url value="/archive/${accession.uuid}" />"><c:out value="${accession.accessionName}" /></a>
            </c:when>
            <c:otherwise>
              <a href="<c:url value="/acn/id/${accession.id}" />"><c:out value="${accession.accessionName}" /></a>
            </c:otherwise>
          </c:choose>
        </div>
        <div class="col-sm-4">
          <span dir="ltr" class="sci-name text-clip"><c:out value="${accession.taxonomy.taxonName}" /></span>
        </div>
        <div class="col-sm-4">
          <a href="https://purl.org/germplasm/id/${accession.uuid}"><c:out value="${accession.uuid}" /></a>
        </div>
      </div>
    </c:forEach>
  </div>
</body>
</html>