<%@ tag description="Make textarea into tinyMCE" pageEncoding="UTF-8" %>
<%@ attribute name="selector" required="true" type="java.lang.String" %>
<%@ attribute name="menubar" required="false" type="java.lang.Boolean" %>
<%@ attribute name="statusbar" required="false" type="java.lang.Boolean" %>
<%@ attribute name="height" required="false" type="java.lang.Integer" %>
<%@ attribute name="directionality" required="false" type="java.lang.String" %>
<%
	if (menubar == null) {
		menubar = false;
	}
	if (statusbar == null) {
		statusbar = false;
	}
	if (height == null) {
		height = 200;
	}
	if (directionality == null) {
		//document.getElementById('editForm').dir
		directionality = "document.dir";
	}
%>
jQuery(document).ready(function () {
  tinyMCE.init({
    selector: "<%= selector %>",
    menubar: <%= menubar %>,
    statusbar: <%= statusbar %>,
    height: <%= height %>,
    plugins: "link autolink code",
    directionality: <%= directionality %>,
    convert_urls: false
  });
});
